#!/bin/sh

dns_target='162.242.211.137 78.46.223.24'

log_file="/tmp/wifi-dns-fix.log"
now=$(date +%Y-%m-%d-%H-%M-%S)

dns_current=$(uci get network.wan.dns)

# if current DNS entry doesn't match the desired one
if [ "$dns_current" != "$dns_target" ]
then
    uci set network.wan.dns="$dns_target"
    uci commit
    /etc/init.d/network reload
    echo "$now updated DNS config, reloaded service" >> $log_file
else
    # or DNS entry is already fixed
    echo "$now DNS is already set to $dns_target" >> $log_file
fi
