#!/bin/sh
if [[ ! -f /etc/openvpn/auth/*.pass && ! -f /etc/openvpn/auth/*.txt ]]; then
    printf "false"
else
    printf "true"
fi
