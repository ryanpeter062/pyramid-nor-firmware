#!/bin/sh
# Created by Bilal Akmal. <mba858@gmail.com>

EMAIL_FILE=$(ls -t /etc/openvpn/auth/*.pass | head -n1)
EMAIL=$(cat $EMAIL_FILE)
RESP=$(curl -k -d "email=$EMAIL" -X POST https://pyramidgo.com/pyramid_api/api/v1/login_update)

#echo $RESP
STATUS=`echo $RESP | sed -e 's/^.*"status"[ ]*:[ ]*"//' -e 's/".*//'`
#echo $STATUS

if [ "$STATUS" == "error" ]; then
	echo "Success message detected"
	$(rm -f /etc/openvpn/auth/*.*)
#else
#	echo "Error message"
fi